import React from "react";
import "./navbar.css";
// BEM -> Block Element Modifier
const Navbar = () => {
  return (
    <div className="gpt3__navbar">
      <div>
        <img
          src="https://mockflow.com/images/home-images2/logonew.png"
          style={{ width: "150px", marginTop: "10px", height: "auto" }}
        />
        Todo List
      </div>
    </div>
  );
};

export default Navbar;
