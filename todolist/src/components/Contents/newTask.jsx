import React, { useEffect, useState } from "react";
import "./contents.css";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DateTimePicker from "@mui/lab/DateTimePicker";
import { useTheme } from "@mui/material/styles";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
let object ={job:'',
  Descriptions:''}
const lc = JSON.stringify([object])
localStorage.setItem('jobs',lc)
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
const names = ["Low", "Normal", "High"];
function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}
let newJobs = JSON.parse(localStorage.getItem("jobs"));
newJobs.pop()
const NewTask = (props) => {
    const [job, setjob] = useState('');
    const [Value, setValue] = useState();
    const [Descriptions, setDescriptions] = useState('');
    const theme = useTheme();
    const [personName, setPersonName] = React.useState([]);
    const handleChange = (event) => {
      const {
        target: { value },
      } = event;
      setPersonName(
        // On autofill we get a the stringified value.
        typeof value === "string" ? value.split(",") : value
      );
    };
     
   
    const handleSubmit = () => {
            if(job.length>0){
              let object ={job:job,
                Descriptions:Descriptions}
                newJobs = [...newJobs, object]
              //save to local storage
              const jsonJobs =JSON.stringify(newJobs)
              localStorage.setItem('jobs', jsonJobs); 
              }
          
        setjob('');
        setDescriptions('');
      console.log(props,"11111111111111111111111111")
        props.setNewData(newJobs)

    }
    return (
        <div className="newtask">
            <div class="row">
          <p>New Task</p>
          <div class="col">
            <input
              class="form-control"
              type="text"
              placeholder="Add a new task"
              required
             minlength="2"
              value={job}
              onChange={e=> setjob(e.target.value)}
            />
            <div class="mb-3">
              <label for="validationTextarea">Descriptions</label>
              <textarea
                class="form-control"
                id="validationTextarea"
                placeholder="Required example textarea"
                required
                value={Descriptions}
                onChange={e=> setDescriptions(e.target.value)}
              ></textarea>
            </div>
            <label for="">Due Date</label> <br />
            <br />
            <div className="dueandpr">
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                  renderInput={(props) => <TextField {...props} />}
                  label="DateTimePicker"
                  value={Value}
                  onChange={(newValue) => {
                    setValue(newValue);
                  }}
                />
              </LocalizationProvider>
              <FormControl sx={{ m: 1, width: 300 }} style={{marginTop:"-23px"}} >
                <label for=""> Priority</label>
                <Select
                  id="demo-multiple-name"
                  value={personName}
                  onChange={handleChange}
                  input={<OutlinedInput label="Name" />}
                  MenuProps={MenuProps}
                  label="priority"
                >
                  {names.map((name) => (
                    <MenuItem
                      key={name}
                      value={name}
                      style={getStyles(name, personName, theme)}
                    >
                      {name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
            <button
             className="btn btn-success active"
             onClick={handleSubmit}
             style={{width:"100%", marginTop:"20px"}}
             >
            Add
            </button>
          </div>
        </div>
        
        </div>
    )
}

export default NewTask
