import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DateTimePicker from "@mui/lab/DateTimePicker";
import { useTheme } from "@mui/material/styles";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
const ShowTask = (props) => {
  const theme = useTheme();
  const [personName, setPersonName] = useState([]);
  const [detailDes, setDetailDes] = useState("");
  const onchangeDes = (e) => {
    setDetailDes(e.target.value);
    console.log(detailDes)
  };
  return (
    <div>
      <ul> 
          <li>
            <div class="form-check">
              <label class="form-check-label">
                <input type="checkbox" class="form-check-input" />
                Do housework
              </label>
              <button className="btn btn-info"
                // onClick={handledetail}
              >Detail</button>
              <button className="btn btn-danger">Remove</button>
            </div>
            <select
            label="Job"
                  class="form-control"
                  // value={detailDes}
                  onChange={onchangeDes}
                >
                  {props.data1 ? props.data1.map((item) => {
                    return <option value={item.Descriptions}>{item.job}</option>;
                  }) : null}
                </select>
            <div class="mb-3">
              <label for="validationTextarea">Descriptions</label>
              {props.data1 ? props.data1.map((item) => {
                return <textarea
                class="form-control"
                id="validationTextarea"
                value={item.Descriptions}
              >
                {item.Descriptions}
              </textarea>
              }) : null}
              
            </div>
            <label for="">Due Date</label> <br />
            <br />
            <div className="dueandpr">
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DateTimePicker
                  renderInput={(props) => <TextField {...props} />}
                  label="DateTimePicker"
                />
              </LocalizationProvider>
              <FormControl
                sx={{ m: 1, width: 300 }}
                style={{ marginTop: "-23px" }}
              >
                <label for=""> Priority</label>
                <Select
                  id="demo-multiple-name"
                  input={<OutlinedInput label="Name" />}
                  MenuProps={MenuProps}
                  label="priority"
                >
                  
                    <MenuItem 
                    >
                    </MenuItem>
                  ))
                </Select>
              </FormControl>
            </div>
          </li>
      
      </ul>
    </div>
  );
};

export default ShowTask;
