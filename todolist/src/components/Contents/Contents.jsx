import React, { useEffect, useState } from "react";
import "./contents.css";
import NewTask from "./newTask";
import Showtask from "./ShowTask";
const Contents = (props) => {
  const [data, setData] = useState('');
  function setNewData(a){
    setData(a)
  }
  return (
    <div>
      <div class="container">
        <div className="row">
        <div className="col-6"><NewTask setNewData={setNewData} /></div>
        <div className="col-6" ><Showtask data1={data} /></div>
        </div>
      </div>
    </div>
  );
};

export default Contents;
