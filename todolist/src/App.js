import React from 'react';
import {Navbar,Header,Footer,Contents} from './components';
import './App.css';
const App = () => {
    return (
        <div className="App">
            <div className="gradient_bg">
                <Navbar/>
                <Header/>
            </div>
            <Contents/>
            <Footer/>
        </div>
    )
}

export default App
